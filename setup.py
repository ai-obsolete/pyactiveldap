#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="pyactiveldap",
    version="0.1",
    description="An ORM for LDAP",
    author="Alessandro Preite Martinez",
    author_email="ale@incal.net",
    url="http://code.autistici.org/trac/pyactiveldap",
    license="MIT",
    platforms=["any"],
    zip_safe=True,
    packages=find_packages(),
)
