# $Id$

from pyactiveldap.ldapobj import LDAPObj


class NotUniqueError(Exception):
    pass


_objectclass_map = {}
class ObjectClassRegistry(type):
    def __new__(meta, classname, bases, classDict):
        t = type.__new__(meta, classname, bases, classDict)
        _objectclass_map[classDict["_class"]] = t
        return t


class Mapper(LDAPObj):
    """Map LDAP objects to a Python objects.

    This class allows to map a Python class to LDAP objects, selected
    by specifying a few class attributes in subclasses. 
    The attributes that every subclass should define are:

      _class:
        The primary objectClass of the LDAP object. All search filters
        include a (objectClass=...) section with this value.

      _key:
        The name of the primary key attribute, used to compute the DN.
        The default key is ``cn''.

      _base_dn:
        The base DN where objects are to be searched for. New objects
        will be created at the level below this one. NOTE that the
        value of this attribute is relative to the base_dn of the
        LDAP session.
        TODO: fancy pluralization a-la-rails, using metaclasses.

      _filter:
        An extra LDAP filter expression that should be applied to
        the searches.

    The following attributes are only used at object creation time:

      _extra_classes:
        An ordered list of extra objectClasses that should be set on
        the new object.

      _defaults:
        A dictionary with attribute value defaults for newly created 
        objects. If a value is a callable, it will be invoked with
        the new object as an argument.

    A very simple example:

        from pyactiveldap.mapper import Mapper
        class PosixGroup(Mapper):
            _class = 'posixGroup'
            _base_dn = 'ou=Groups'
            _defaults = {'description': lambda x: x.cn}
        g = PosixGroup.find(db, 'wheel')
        print g.gidNumber
        ...

    """

    __metaclass__ = ObjectClassRegistry

    _class = None
    _extra_classes = None
    _key = "cn"
    _base_dn = None
    _filter = ""
    _defaults = {}

    def __repr__(self):
        return "<%s: %s>" % (self.__class__.__name__,
                             getattr(self, self._key))

    @classmethod
    def new(cls, db, **attrs):
        """Create a new instance.

        The instance attributes, passed as keyword arguments, must
        allow for determining the DN, either by specifying the primary
        key attribute, or by directly passing the ``dn'' attribute.
        In the latter case, no check is done to ensure that it falls
        below the class base_dn.
        The DN can also be determined by hierarchy, if the ``parent''
        attribute is specified: in this case, the resulting object 
        will be a child of it.

        Raises ValueError if it can't figure out the DN, or
        NotUniqueError if an object with the same DN already exists in
        the database.
        """
        # Find what the new DN should be
        if "dn" in attrs:
            dn = attrs["dn"]
            del attrs["dn"]
        elif "parent" in attrs:
            parent = attrs["parent"]
            del attrs["parent"]
            dn = "%s=%s,%s" % (cls._key, key, parent.dn)
        else:
            if cls._key not in attrs:
                raise ValueError("Key not specified")
            key = attrs[cls._key]
            dn = "%s=%s,%s,%s" % (
                cls._key, key, cls._base_dn, db.base_dn)

        # Check if the object already exists
        if db.exists(dn):
            raise NotUniqueError("Not unique")

        # Create the object and set default parameters
        attrs["objectClass"] = ["top", cls._class]
        if cls._extra_classes:
            attrs["objectClass"] += cls._extra_classes
        obj = cls(dn, db.session, **attrs)
        for key, value in cls._defaults.items():
            if not key in attrs:
                if callable(value):
                    setattr(obj, key, value(obj))
                else:
                    setattr(obj, key, value)

        return obj

    @classmethod
    def find(cls, db, key):
        """Find a single object identified by the given key.

        Will return None if no such object was found.
        """
        base = "%s,%s" % (cls._base_dn, db.base_dn)
        filt = "(&(%s=%s)(objectClass=%s)%s)" % (
            cls._key, key, cls._class, cls._filter)
        res = db.search(filt, base)
        if res:
            return res.next()

    @classmethod
    def find_all(cls, db, **attrs):
        """Find all objects that satisfy a criteria.

        Any keyword arguments will be ANDed to the default filter (as
        equality comparisons).
        """
        base = "%s,%s" % (cls._base_dn, db.base_dn)
        attrs_filter = "".join(
            ["(%s=%s)" % (k, v) for k, v in attrs.items()])
        f = "(&(objectClass=%s)%s%s)" % (
            cls._class, cls._filter, attrs_filter)
        return db.search(f, base)

    def find_sub(cls, db, **attrs):
        """Find all children of this object that meet a criteria.

        Performs a SCOPE_ONE search with the current object as base. 
        Any keyword arguments will be ANDed to the default filter (as
        equality comparisons). Returns an iterator that yields
        objects of the mapped classes.
        """
        f = "".join(
            ["(%s=%s)" % (k, v) for k, v in attrs.items()])
        if cls._filter:
            f = "(&%s%s)" % (cls._filter, f)
        res = self._session.ldap.search_s(
            self.dn, ldap.SCOPE_ONELEVEL, f, ["dn", "objectClass"])
        for dn, data in res:
            class_ = LDAPObj
            for obj_class in data["objectClass"]:
                if obj_class in _objectclass_map:
                    class_ = _objectclass_map[obj_class]
                    break
            cobj = class_(dn, self._session)
            yield cobj

