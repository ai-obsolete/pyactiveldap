# $Id$
# <ale@incal.net>

import ldap
import ldif
from cStringIO import StringIO
from ldap.functions import explode_dn


def _tostr(x):
    if isinstance(x, unicode):
        return x.encode('utf-8')
    elif isinstance(x, str):
        return x
    else:
        return str(x)


class LDAPObj(object):
    """A transparent dictionary wrapper for LDAP objects.

    LDAP attributes are mapped to object attributes (note: case is
    preserved), so you can use the LDAP object as a normal Python
    object.  Multi-valued attributes are supported with the append()
    and get_all() methods.

    The class supports serialization to and from LDIF using the
    to_ldif() and from_ldif() methods.
    """

    def __init__(self, dn=None, session=None, **kwargs):
	self._session = session
        self._new = True
	self.dn = dn
	if dn:
	    self.rdn = explode_dn(dn)[0]
	self._data = None
	for k, v in kwargs.items():
	    setattr(self, k, v)

    def __repr__(self):
	return "<LDAPObj: %s>" % self.dn

    def _update_data(self):
	if self._data != None or not self.dn:
            return
        self._data = {}
        try:
            res = self._session.ldap.search_s(self.dn, ldap.SCOPE_BASE)
            self._data = res[0][1]
            self._new = False
        except ldap.NO_SUCH_OBJECT:
            self._new = True

    def is_new(self):
        """Returns True if the object is not yet present in the
        database."""
	self._update_data()
	return self._new

    def is_class(self, class_):
        """Returns True if the object has the specified objectClass."""
        return (class_ in self.get_all("objectClass"))

    def __getattr__(self, k):
        if k in self.__dict__:
            return self.__dict__[k]
	self._update_data()
        return self._data.get(k, [None])[0]

    def get_all(self, k):
        """Returns all the values for the given key (for multi-valued
        attributes)."""
	self._update_data()
	return self._data.get(k, [])

    def delattr(self, k):
        """Clear an attribute, if present."""
	self._update_data()
        del self._data[k]
        self._session.add_mod(self.dn, (ldap.MOD_DELETE, k, None))

    def __setattr__(self, k, v):
	"""Sets the value of attribute ``k''. 

        Value ``v'' can either be a scalar or a list of values (for
	multiple-valued attributes).  Setting an attribute to None is
	the same thing as calling delattr(k), i.e. it will remove the
	attribute from the object.
	"""
        if k[0] == "_" or k in ("dn", "rdn"):
            self.__dict__[k] = v
            return

        if v is None or v == []:
            return self.delattr(k)

        op = ldap.MOD_ADD
	self._update_data()
        if k in self._data:
 	    if len(self._data[k]) > 1:
	        self._session.add_mod(self.dn, (ldap.MOD_DELETE, k, None))
	    else:
                op = ldap.MOD_REPLACE
	if isinstance(v, list):
            v = map(_tostr, v)
	    self._data[k] = v
	else:
            v = _tostr(v)
	    self._data[k] = [v]
	self._session.add_mod(self.dn, (op, k, v))

    def append(self, k, v):
        """Adds a value to a multiple-valued attribute."""
	self._update_data()
        v = _tostr(v)
        self._data.setdefault(k, []).append(v)
        self._session.add_mod(self.dn, (ldap.MOD_ADD, k, v))

    def parent(self):
	"""Returns the parent object (if any)."""
	parts = explode_dn(self.dn)
	if len(parts) > 1:
	    return LDAPObj(",".join(parts[1:]), self._session)
	else:
	    return None

    def search_one(self, filt="(objectClass=*)"):
	"""Returns the next-level children of this object.

	It actually just calls an LDAP search with scope=one. The
        function returns an iterator.
	"""
        res = self._session.ldap.search_s(
            self.dn, ldap.SCOPE_ONELEVEL, filt, ["dn"])
        if res:
            return (LDAPObj(x[0], self._session) for x in res)
        else:
            return []

    def search(self, filt="(objectClass=*)"):
	"""Returns all the sub-objects of this one. 

        You can (and should) specify a filter for the search. The
        function returns an iterator.
	"""
        res = self._session.ldap.search_s(
            self.dn, ldap.SCOPE_SUBTREE, filt, ["dn"])
        if res:
            return (LDAPObj(x[0], self._session) for x in res)

    def to_ldif(self):
	"""Serialize the object data to LDIF format."""
	self._update_data()
	buf = StringIO()
        # 'dn' appears in self._data as well, strip it from the output.
        stripped_data = self._data.copy()
        del stripped_data["dn"]
	ldif.LDIFWriter(buf).unparse(self.dn, stripped_data)
	return buf.getvalue()

    def from_ldif(self, ldif_data):
	"""Parse LDIF data into the current object. 

        Note that the LDIF must be relative to this single object. All
	pre-existing attributes will be cleared.
	"""
	class _ldif_parser(ldif.LDIFParser):
	    def __init__(self, input, outobj):
		ldif.LDIFParser.__init__(self, input)
		self.outobj = outobj
		self.new_keys = []
	    def handle(self, dn, record):
		if not self.outobj.dn:
		    self.outobj.dn = dn
		    self.outobj.rdn = explode_dn(self.outobj.dn)[0]
		if self.outobj.dn != dn:
		    raise Exception("Trying to load more than one object!")
		for k, v in record.iteritems():
		    setattr(self.outobj, k, v)
		    self.new_keys.append(k)
	    def cleanup(self):
                old_keys = [x for x in self.outobj._data
                            if x not in self.new_keys]
		for k in old_keys:
                    self.outobj.delattr(k)
	p = _ldif_parser(StringIO(ldif_data), self)
	p.parse()
	p.cleanup()

    def delete(self):
	"""Remove this object and all its children.

        This call will skip the current Session, the object will be
        immediately removed from the database. Any pending changes for
        this DN in the current transaction will be cleared.
        """
	children = self.search_one()
        if children:
            for x in children:
                x.delete()
        if self.dn in self._session.changes:
            del self._session.changes[self.dn]
	self._session.ldap.delete_s(self.dn)

    def _to_modlist(self, modlist=None, recurse=1):
	self._update_data()
	if not modlist:
	    modlist = []
	t = [(ldap.MOD_ADD, k, v) for k, v in self._data.iteritems()]
	modlist.append((self.dn, t))
	for x in self.search_one():
	    x._to_modlist(modlist)
	return modlist
	
    def rename(self, new_dn):
	"""Rename this object to a new DN. 

        If you change the RDN the corresponding attribute will be
	modified.  If the object has no children, the ldap RENAME
	command will be used, otherwise the objects will be created
	under the new DN and then deleted.

        This code will skip the current Session, changes will be
        immediately effective in the database.
	"""
        self._update_data()
	old_dn_parts = explode_dn(self.dn)
	new_dn_parts = explode_dn(new_dn)
	old_parent = ",".join(old_dn_parts[1:])
	new_parent = ",".join(new_dn_parts[1:])
	new_rdn = new_dn_parts[0]
        new_rdn_parts = new_rdn.split("=", 1)

	# serialize this object and all its sub-objects
	def change_dn(old_top_parts, new_top_parts, dn):
	    dn_parts = [x.lower() for x in explode_dn(dn)]
	    parts_to_keep = len(dn_parts) - len(old_top_parts)
	    return ",".join(dn_parts[0:parts_to_keep] + new_top_parts)

	# se l'oggetto e' terminale (una foglia), utilizza
	# il comando LDAP rename()...
	if not self.search_one():
	    if old_parent == new_parent:
		self._session.ldap.rename_s(self.dn, new_rdn)
	    else:
		self._session.ldap.rename_s(self.dn, new_rdn, new_parent)
	else:
	    # serializza questo oggetto e tutti i suoi figli
	    # affinche' dopo si possano reintegrare in una 
	    # serie di comandi LDAP add() -- nota il cambiamento
	    # esplicito dell'attributo dell'RDN...
	    modlist = [(change_dn(old_dn_parts, new_dn_parts, x[0]), x[1]) 
		       for x in self._to_modlist()]
	    for m in modlist:
		self._session.ldap.modify_ext_s(m[0], m[1])
	    self.delete()

        self.dn = new_dn
        self.rdn = new_rdn
        self._data[new_rdn_parts[0]] = [new_rdn_parts[1]]


