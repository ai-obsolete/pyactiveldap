# $Id$

import ldap
import mox
import unittest
from pyactiveldap import session

TEST_BASE_DN = "dc=test"


class TestSession(unittest.TestCase):

    def setUp(self):
        self.mox = mox.Mox()
        self.ldap = self.mox.CreateMockAnything()
        self.s = session.Session(self.ldap)
        self.dn = "ou=Test," + TEST_BASE_DN

    def tearDown(self):
        self.mox.UnsetStubs()

    def testAutoCommit(self):
        self.s.auto_commit = True
        change = (ldap.MOD_ADD, "ou", "Test")
        self.ldap.modify_s(self.dn, [change]).AndReturn(None)
        self.mox.ReplayAll()
        self.s.add_mod(self.dn, change)
        self.mox.VerifyAll()

    def testSimpleModChange(self):
        change = (ldap.MOD_ADD, "ou", "Test")
        self.ldap.modify_s(self.dn, [change]).AndReturn(None)
        self.mox.ReplayAll()
        self.s.add_mod(self.dn, change)
        self.s.commit()
        self.mox.VerifyAll()

    def testSimpleAddChange(self):
        change = (ldap.MOD_ADD, "ou", "Test")
        self.ldap.modify_s(self.dn, [change]).AndRaise(ldap.NO_SUCH_OBJECT)
        self.ldap.add_s(self.dn, [("ou", "Test")]).AndReturn(None)
        self.mox.ReplayAll()
        self.s.add_mod(self.dn, change)
        self.s.commit()
        self.mox.VerifyAll()

    def testChangeTwiceTheSameAttribute(self):
        change = (ldap.MOD_ADD, "ou", "Test")
        change2 = (ldap.MOD_ADD, "ou", "Test2")
        self.ldap.modify_s(self.dn, [change2]).AndReturn(None)
        self.mox.ReplayAll()
        self.s.add_mod(self.dn, change)
        self.s.add_mod(self.dn, change2)
        self.s.commit()
        self.mox.VerifyAll()

    def testSetAndDeleteAttribute(self):
        change = (ldap.MOD_ADD, "ou", "Test")
        change2 = (ldap.MOD_DELETE, "ou", None)
        self.ldap.modify_s(self.dn, [change2]).AndReturn(None)
        self.mox.ReplayAll()
        self.s.add_mod(self.dn, change)
        self.s.add_mod(self.dn, change2)
        self.s.commit()
        self.mox.VerifyAll()

    def testRollback(self):
        change = (ldap.MOD_ADD, "ou", "Test")
        change2 = (ldap.MOD_DELETE, "ou", None)
        self.mox.ReplayAll()
        self.s.add_mod(self.dn, change)
        self.s.add_mod(self.dn, change2)
        self.s.rollback()
        self.mox.VerifyAll()



if __name__ == "__main__":
    unittest.main()

