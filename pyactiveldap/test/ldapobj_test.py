# $Id$

import ldap
import mox
import unittest
from pyactiveldap import session
from pyactiveldap import ldapobj

TEST_BASE_DN = "dc=test"
TEST_DN = "ou=Test,%s" % TEST_BASE_DN

def _gen_test_data(dn=TEST_DN):
    return [
        [dn, {
                "dn": [dn], "ou": ["Test"],
                "objectClass": ["top", "testclass"],
                "testattr": ["testvalue"],
                "listattr": ["a", "b"],
            }]
         ]


class TestSession(unittest.TestCase):

    def setUp(self):
        self.mox = mox.Mox()
        self.ldap = self.mox.CreateMockAnything()
        self.s = session.Session(self.ldap)
        self.dn = "ou=Test," + TEST_BASE_DN

    def tearDown(self):
        self.mox.UnsetStubs()

    def testObjectCreationWithDn(self):
        obj = ldapobj.LDAPObj(self.dn, self.s)
        self.assertEquals(self.dn, obj.dn)
        self.assertEquals("ou=Test", obj.rdn)
        self.assertEquals(self.s, obj._session)

    def testObjectCreation(self):
        obj = ldapobj.LDAPObj(None, self.s)
        self.assertEquals(None, obj.dn)

    def testGetAttribute(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        self.assertEquals(None, obj.non_existing_attr)
        self.assertEquals("testvalue", obj.testattr)
        self.assertEquals(["top", "testclass"], obj.get_all("objectClass"))
        self.mox.VerifyAll()

    def testSetAttribute(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.modify_s(self.dn, [
                (ldap.MOD_ADD, "description", "test")])
        self.mox.ReplayAll()

        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.description = "test"
        self.s.commit()
        self.assertEquals("test", obj.description)

        self.mox.VerifyAll()

    def testSetUTF8Attribute(self):
        utf8text = u'b\xe4a'
        utf8text_enc = utf8text.encode('utf-8')
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.modify_s(self.dn, [
                (ldap.MOD_ADD, "description", utf8text_enc)])
        self.mox.ReplayAll()

        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.description = utf8text
        self.s.commit()
        self.assertEquals(utf8text_enc, obj.description)

        self.mox.VerifyAll()

    def testSetAttributeThatAlreadyExists(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.modify_s(self.dn, [
                (ldap.MOD_REPLACE, "testattr", "othervalue")])
        self.mox.ReplayAll()

        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.testattr = "othervalue"
        self.s.commit()
        self.assertEquals("othervalue", obj.testattr)

        self.mox.VerifyAll()

    def testDeleteAttribute(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.modify_s(self.dn, [
                (ldap.MOD_DELETE, "testattr", None)])
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.delattr("testattr")
        def delNonExistingAttr():
            obj.delattr("nonexistingattribute")
        self.assertRaises(KeyError, delNonExistingAttr)
        self.s.commit()
        self.mox.VerifyAll()

    def testSetAttributeToNone(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.modify_s(self.dn, [
                (ldap.MOD_DELETE, "testattr", None)])
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.testattr = None
        self.s.commit()
        self.mox.VerifyAll()

    def testSetAttributeToList(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.modify_s(self.dn, [
                (ldap.MOD_ADD, "listattr2", ["a", "b"])])
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.listattr2 = ["a", "b"]
        self.s.commit()
        self.mox.VerifyAll()

    def testSetAttributeToEmptyList(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.modify_s(self.dn, [
                (ldap.MOD_DELETE, "listattr", None)])
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.listattr = []
        self.s.commit()
        self.mox.VerifyAll()

    def testSetNonExistingAttributeToEmptyList(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        def set_to_empty_list():
            obj.listattr2 = []
        self.assertRaises(KeyError, set_to_empty_list)
        self.mox.VerifyAll()

    def testSetAttributeThatIsAlreadyAList(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.modify_s(self.dn, [
                (ldap.MOD_DELETE, "listattr", None),
                (ldap.MOD_ADD, "listattr", "c")])
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.listattr = "c"
        self.s.commit()
        self.assertEquals("c", obj.listattr)
        self.mox.VerifyAll()

    def testAppendAttribute(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.modify_s(self.dn, [
                (ldap.MOD_ADD, "testattr", "testvalue2"),
                (ldap.MOD_ADD, "listattr", "c")])
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.append("testattr", "testvalue2")
        obj.append("listattr", "c")
        self.s.commit()
        self.assertEquals(["testvalue", "testvalue2"],
                          obj.get_all("testattr"))
        self.assertEquals(["a", "b", "c"],
                          obj.get_all("listattr"))
        self.mox.VerifyAll()

    def testDeleteAndThenSetAttribute(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.modify_s(self.dn, [
                (ldap.MOD_DELETE, "testattr", None),
                (ldap.MOD_ADD, "testattr", "testvalue2")])
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.delattr("testattr")
        obj.testattr = "testvalue2"
        self.s.commit()
        self.mox.VerifyAll()

    def testExistingObjectIsNotNew(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        self.assertEquals(False, obj.is_new())
        self.mox.VerifyAll()

    def testNewObjectIsNew(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndRaise(
            ldap.NO_SUCH_OBJECT)
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        self.assertEquals(True, obj.is_new())
        self.mox.VerifyAll()

    def testIsClass(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        self.assertEquals(True, obj.is_class("testclass"))
        self.assertEquals(False, obj.is_class("wrongclass"))
        self.mox.VerifyAll()

    def testParent(self):
        obj = ldapobj.LDAPObj(self.dn, self.s)
        parent1 = obj.parent()
        self.assertTrue(parent1 is not None)
        parent2 = obj.parent()
        self.assertTrue(parent2 is not None)

    def testSearchOne(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_ONELEVEL, 
                           "(objectClass=*)", ["dn"])
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        res = obj.search_one()
        self.mox.VerifyAll()

    def testSearchNoResults(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_SUBTREE, 
                           "(objectClass=*)", ["dn"]).AndReturn(None)
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        res = obj.search()
        self.mox.VerifyAll()

    def testSearch(self):
        child_dn = "cn=something," + self.dn
        self.ldap.search_s(self.dn, ldap.SCOPE_SUBTREE,
			   "(objectClass=*)", ["dn"]
			  ).AndReturn(_gen_test_data(child_dn))
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        res = list(obj.search())
        self.assertEquals(1, len(res))
        self.assertEquals(child_dn, res[0].dn)
        self.mox.VerifyAll()

    def testToAndFromLdif(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        ldif = obj.to_ldif()
        self.assert_(self.dn in ldif)
        self.assert_("testvalue" in ldif)

        obj2 = ldapobj.LDAPObj(None, self.s)
        obj2.from_ldif(ldif)
        self.assertEquals(obj.dn, obj2.dn)
        self.assertEquals(obj.rdn, obj2.rdn)
        self.assertEquals(obj.testattr, obj2.testattr)
        self.mox.VerifyAll()

    def testFromLdifFailsOnMoreThanOneObject(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(None, self.s)
        ldif = """
dn: ou=Test,dc=test
ou: Test
objectClass: top
testattr: testvalue

dn: ou=AnotherTest,dc=test
ou: AnotherTest
objectClass: top
"""
        self.assertRaises(Exception, lambda: obj.from_ldif(ldif))
        self.mox.VerifyAll()

    def testDeleteObjectWithPendingChanges(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.search_s(self.dn, ldap.SCOPE_ONELEVEL,
                           "(objectClass=*)", ["dn"]).AndReturn([])
        self.ldap.delete_s(self.dn)
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.testattr =  "othervalue"
        obj.anotherattr = "anothervalue"
        obj.delete()
        self.s.commit()
        self.mox.VerifyAll()

    def testDeleteObjectTree(self):
        self.ldap.search_s(self.dn, ldap.SCOPE_ONELEVEL,
                           "(objectClass=*)", ["dn"]).AndReturn([
            ("cn=something,ou=Test,dc=test", ),
            ("cn=something2,ou=Test,dc=test", )])
        self.ldap.search_s("cn=something,ou=Test,dc=test",
                           ldap.SCOPE_ONELEVEL, "(objectClass=*)", ["dn"])
        self.ldap.delete_s("cn=something,ou=Test,dc=test")
        self.ldap.search_s("cn=something2,ou=Test,dc=test",
                           ldap.SCOPE_ONELEVEL, "(objectClass=*)", ["dn"])
        self.ldap.delete_s("cn=something2,ou=Test,dc=test")
        self.ldap.delete_s(self.dn)
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.delete()
        self.s.commit()
        self.mox.VerifyAll()

    def testRenameSimpleObject(self):
        new_dn = "ou=Test2,dc=test"
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.search_s(self.dn, ldap.SCOPE_ONELEVEL, 
                           "(objectClass=*)", ["dn"])
        self.ldap.rename_s(self.dn, "ou=Test2")
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.rename(new_dn)
        self.mox.VerifyAll()

        self.assertEquals(new_dn, obj.dn)
        self.assertEquals("ou=Test2", obj.rdn)
        self.assertEquals("Test2", obj.ou)

    def testRenameObjectToADifferentParent(self):
        new_dn = "ou=Test2,dc=test2"
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(_gen_test_data())
        self.ldap.search_s(self.dn, ldap.SCOPE_ONELEVEL, 
                           "(objectClass=*)", ["dn"])
        self.ldap.rename_s(self.dn, "ou=Test2", "dc=test2")
        self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.rename(new_dn)
        self.mox.VerifyAll()

        self.assertEquals(new_dn, obj.dn)
        self.assertEquals("ou=Test2", obj.rdn)
        self.assertEquals("Test2", obj.ou)

    def testRenameObjectTree(self):
        new_dn = "ou=Test2,dc=test"
        child_dn = "cn=something," + self.dn
        new_child_dn = "cn=something," + new_dn

	# Return simpler (altough invalid) data, so that we can
	# check the results more easily.
        self.ldap.search_s(self.dn, ldap.SCOPE_BASE).AndReturn(
            [(self.dn, {"ou": ["Test"]})])
        self.ldap.search_s(self.dn, ldap.SCOPE_ONELEVEL,
                           "(objectClass=*)", ["dn"]
                          ).AndReturn([(child_dn, )])

	# _to_modlist() is called recursively...
        self.ldap.search_s(self.dn, ldap.SCOPE_ONELEVEL,
                           "(objectClass=*)", ["dn"]
                          ).AndReturn([(child_dn, )])
        self.ldap.search_s(child_dn, ldap.SCOPE_BASE).AndReturn(
            [(child_dn, {"cn": ["something"]})])
        self.ldap.search_s(child_dn, ldap.SCOPE_ONELEVEL,
                           "(objectClass=*)", ["dn"]
                          ).AndReturn(None)

	# creation of the new objects (in-order)
	self.ldap.modify_ext_s(new_dn, [(ldap.MOD_ADD, "ou", ["Test"])])
	self.ldap.modify_ext_s(new_child_dn, [(ldap.MOD_ADD, "cn", ["something"])])

	# deletion of the old objects
        self.ldap.search_s(self.dn, ldap.SCOPE_ONELEVEL,
                           "(objectClass=*)", ["dn"]
                          ).AndReturn([(child_dn, )])
        self.ldap.search_s(child_dn, ldap.SCOPE_ONELEVEL,
                           "(objectClass=*)", ["dn"])
        self.ldap.delete_s(child_dn)
        self.ldap.delete_s(self.dn)

	self.mox.ReplayAll()
        obj = ldapobj.LDAPObj(self.dn, self.s)
        obj.rename(new_dn)
	self.mox.VerifyAll()


if __name__ == "__main__":
    unittest.main()
