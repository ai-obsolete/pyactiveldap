# $Id$

import ldap
import mox
import unittest
from pyactiveldap import connection
from pyactiveldap import mapper

TEST_BASE_DN = "dc=test"

def _gen_test_data(dn):
    return [
        [dn, {
                "dn": [dn], "cn": ["test"],
                "objectClass": ["top", "testclass"],
                "testattr": ["testvalue"],
                "listattr": ["a", "b"],
            }]
         ]


class ConnectionMock(connection.Connection):

    def connect(self, ldap_mock):
        return ldap_mock


class SimpleMappedClass(mapper.Mapper):
    _key = "cn"
    _class = "testclass"
    _base_dn = "ou=MyClasses"
    _defaults = {"testattr": "testvalue",
                 "derivedattr": lambda x: "42"}
    _extra_classes = ["otherclass"]


class TestMapper(unittest.TestCase):

    def setUp(self):
        self.mox = mox.Mox()
        self.ldap = self.mox.CreateMockAnything()
        self.conn = ConnectionMock(TEST_BASE_DN, self.ldap)

    def tearDown(self):
        self.mox.UnsetStubs()

    def testFind(self):
        expected_filter = "(&(cn=test)(objectClass=testclass))"
        expected_base = "ou=MyClasses,dc=test"
        test_dn = "cn=test," + expected_base
        self.ldap.search_s(
            expected_base, ldap.SCOPE_SUBTREE, expected_filter, ["dn"]
            ).AndReturn(_gen_test_data(test_dn))
        self.ldap.search_s(test_dn, ldap.SCOPE_BASE).AndReturn(
            _gen_test_data(test_dn))
            
        self.mox.ReplayAll()

        res = SimpleMappedClass.find(self.conn, "test")
        self.assertEquals("test", res.cn)
        self.assertEquals(test_dn, res.dn)

        self.mox.VerifyAll()

    def testFindNonexistingObject(self):
        expected_filter = "(&(cn=test)(objectClass=testclass))"
        expected_base = "ou=MyClasses,dc=test"
        test_dn = "cn=test," + expected_base
        self.ldap.search_s(
            expected_base, ldap.SCOPE_SUBTREE, expected_filter, ["dn"]
            ).AndReturn(None)
        self.mox.ReplayAll()
        res = SimpleMappedClass.find(self.conn, "test")
        self.assertEquals(None, res)
        self.mox.VerifyAll()

    def testFindAll(self):
        expected_filter = "(&(objectClass=testclass))"
        expected_base = "ou=MyClasses,dc=test"
        test_dn = "cn=test," + expected_base
        self.ldap.search_s(
            expected_base, ldap.SCOPE_SUBTREE, expected_filter, ["dn"]
            ).AndReturn(_gen_test_data(test_dn))
        self.mox.ReplayAll()

        res = list(SimpleMappedClass.find_all(self.conn))
        self.assertEquals(1, len(res))
        self.assertEquals(test_dn, res[0].dn)

        self.mox.VerifyAll()

    def testFindAllWithAttributes(self):
        expected_filter = "(&(objectClass=testclass)(testattr=testvalue))"
        expected_base = "ou=MyClasses,dc=test"
        test_dn = "cn=test," + expected_base
        self.ldap.search_s(
            expected_base, ldap.SCOPE_SUBTREE, expected_filter, ["dn"]
            ).AndReturn(_gen_test_data(test_dn))
        self.mox.ReplayAll()

        res = list(SimpleMappedClass.find_all(self.conn,
                                              testattr="testvalue"))
        self.assertEquals(1, len(res))
        self.assertEquals(test_dn, res[0].dn)
        
        self.mox.VerifyAll()

    def testCreateNewObject(self):
        expected_dn = "cn=test2,ou=MyClasses,dc=test"
        self.ldap.search_s(expected_dn, ldap.SCOPE_BASE,
                           "(objectClass=*)", ["dn"]
                           ).AndRaise(ldap.NO_SUCH_OBJECT)
        self.ldap.search_s(expected_dn, ldap.SCOPE_BASE
                           ).AndRaise(ldap.NO_SUCH_OBJECT)
        self.mox.ReplayAll()

        obj = SimpleMappedClass.new(self.conn, cn="test2")
        self.assertEquals(expected_dn, obj.dn)
        self.assertTrue(obj.is_class("testclass"))
        self.assertTrue(obj.is_class("otherclass"))
        self.assertEquals("testvalue", obj.testattr)
        self.assertEquals("42", obj.derivedattr)

        self.mox.VerifyAll()

    def testCreateNewObjectFailsWithoutPrimaryKey(self):
        self.assertRaises(ValueError,
                          lambda: SimpleMappedClass.new(self.conn,
                                                        uselessAttr="test"))

    def testCreateExistingObject(self):
        expected_dn = "cn=test2,ou=MyClasses,dc=test"
        self.ldap.search_s(expected_dn, ldap.SCOPE_BASE,
                           "(objectClass=*)", ["dn"]
                           ).AndReturn(_gen_test_data(expected_dn))
        self.mox.ReplayAll()
        self.assertRaises(mapper.NotUniqueError,
                          lambda: SimpleMappedClass.new(self.conn, cn="test2"))
        self.mox.VerifyAll()
        
    def testCreateNewObjectWithExplicitDN(self):
        expected_dn = "cn=test2,ou=MyClasses,dc=test"
        self.ldap.search_s(expected_dn, ldap.SCOPE_BASE,
                           "(objectClass=*)", ["dn"]
                           ).AndRaise(ldap.NO_SUCH_OBJECT)
        self.ldap.search_s(expected_dn, ldap.SCOPE_BASE
                           ).AndRaise(ldap.NO_SUCH_OBJECT)
        self.mox.ReplayAll()
        obj = SimpleMappedClass.new(self.conn, dn=expected_dn,
                                    cn="test2")
        self.assertEquals(expected_dn, obj.dn)
        self.assertEquals("test2", obj.cn)
        self.mox.VerifyAll()


if __name__ == "__main__":
    unittest.main()
