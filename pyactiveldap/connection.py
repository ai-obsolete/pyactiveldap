# $Id$

import ldap
from ldap.ldapobject import ReconnectLDAPObject
from pyactiveldap import ldapobj
from pyactiveldap import session


class Connection(object):
    """LDAP connection object.

    Wraps together a Session object with an LDAP connection, providing
    some useful database-wide methods. The connection implementation
    is isolated in a separate function for easier subclassing.
    """

    def __init__(self, base_dn, *args, **kwargs):
        """Constructor.

	Any argument besides ``base_dn'' will be passed to connect().
        """
        self.base_dn = base_dn
        self.session = session.Session(self.connect(*args, **kwargs))

    def connect(self, uri, bind_dn=None, bind_pw=None):
        """Create a connection to the database.

        The default implementation uses a reconnecting ldap connector
        and uses the simple ldap authentication.

        Override this method in a subclass if you need to provide a
	different LDAP connection object, or an authentication method
	that is different from the default.
	"""
	conn = ReconnectLDAPObject(uri)
        if bind_dn and bind_pw:
            conn.simple_bind_s(bind_dn, bind_pw)
	return conn

    def new(self, dn=None, **kwargs):
        """Create a new LDAP object."""
        return ldapobj.LDAPObj(dn, self.session, **kwargs)

    def search(self, filter, base=None):
        """Search the database with the given filter.

	Returns an iterator.
	"""
        if not base:
            base = self.base_dn
        result = self.session.ldap.search_s(
            base, ldap.SCOPE_SUBTREE, filter, ["dn"])
        if result:
            return (ldapobj.LDAPObj(x[0], self.session) for x in result)
        else:
            return []

    def exists(self, dn):
        """Return True if the specified dn exists."""
        try:
            self.session.ldap.search_s(
                dn, ldap.SCOPE_BASE, "(objectClass=*)", ["dn"])
            return True
        except ldap.NO_SUCH_OBJECT:
            return False

    def commit(self):
        """Commit all the pending changes."""
        self.session.commit()

    def rollback(self):
        """Roll back all the pending changes."""
        self.session.rollback()
