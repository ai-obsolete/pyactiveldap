# $Id$
# <ale@incal.net>

import ldap
from ldap.ldapobject import ReconnectLDAPObject


class RWLDAPObject(ReconnectLDAPObject):
    """An LDAP Object class that supports master/slave operation.

    This connector will send all the search requests to the local server
    (the one specified with the ``uri'' parameter), and all the updates
    to the master server.
    """

    def __init__(self, uri, master_uri = None):
	ReconnectLDAPObject.__init__(self, uri)
	self.protocol_version = ldap.VERSION3
	self.master_c = None
        self.master_uri = master_uri
        self.bind_dn = None
        self.bind_pw = None

    def _bind_master(self):
        if self.bind_dn:
            try:
                self.master_c.simple_bind_s(self.bind_dn, self.bind_pw)
            except ldap.SERVER_DOWN:
                self.master_c.reconnect(self.master_c._uri)
                self.master_c.simple_bind_s(self.bind_dn, self.bind_pw)

    def _connect_master(self):
	if self.master_uri and not self.master_c:
	    self.master_c = ReconnectLDAPObject(self.master_uri)
	    self.master_c.protocol_version = ldap.VERSION3
            if self.bind_dn:
                self._bind_master()

    def simple_bind_s(self, dn, passwd):
        self.bind_dn = dn
        self.bind_pw = passwd
	if self.master_c:
            self._bind_master()
        # correct this missing behavior in ReconnectLDAPObject
        try:
            ReconnectLDAPObject.simple_bind_s(self, dn, passwd)
        except ldap.SERVER_DOWN:
            self.reconnect(self._uri)
            ReconnectLDAPObject.simple_bind_s(self, dn, passwd)

    def add_s(self, dn, data):
        self._connect_master()
	if self.master_c:
	    return self.master_c.add_s(dn, data)
	else:
	    return ReconnectLDAPObject.add_s(self, dn, data)
    
    def modify_s(self, dn, data):
        self._connect_master()
	if self.master_c:
	    return self.master_c.modify_ext_s(dn, data)
	else:
	    return ReconnectLDAPObject.modify_ext_s(self, dn, data)

    def rename_s(self, old_dn, new_rdn, new_parent=None):
        self._connect_master()
	if self.master_c:
	    return self.master_c.rename_s(old_dn, new_rdn, new_parent)
	else:
	    return ReconnectLDAPObject.rename_s(self, old_dn, new_rdn, new_parent)

    def delete_s(self, dn):
        self._connect_master()
	if dn in self.changes:
	    del self.changes[dn]
	try:
	    if self.master_c:
		return self.master_c.delete_s(dn)
	    else:
		return ReconnectLDAPObject.delete_s(self, dn)
	except ldap.NO_SUCH_OBJECT:
	    return

