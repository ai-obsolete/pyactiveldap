# $Id$
# <ale@incal.net>

from pyactiveldap.connection import Connection
from pyactiveldap.contrib.rwldapobj import RWLDAPObject


class RWConnection(Connection):
    """Master/slave split LDAP connection."""

    def connect(self, uri, master_uri=None, bind_dn=None, bind_pw=None):
        conn = RWLDAPObject(uri, master_uri)
        if bind_dn and bind_pw:
            conn.simple_bind_s(bind_dn, bind_pw)
        return conn
