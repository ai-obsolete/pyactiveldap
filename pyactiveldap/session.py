# $Id$

import ldap
import logging


class Session(object):
    """A very simple transaction.

    A Session stores pending changes to the LDAP database (a
    "transaction", even if in a very broad acception), so that you can
    commit them at once, or rollback.

    Note that this is a very simple implementation, it just aims at
    providing the commit/rollback functionality, but it doesn't
    guarantee atomicity, and it is transparent to lookups: until you
    call commit(), changes won't be visible at all.

    The Session will preserve order of changes relatively to each
    specific DN: the order of DNs is random, but for each DN changes
    will be applied in order. This is enough to prevent violating
    schema constraints.
    """

    def __init__(self, ldap_conn, auto_commit=False):
        self.ldap = ldap_conn
        self.changes = {}
        self.auto_commit = auto_commit

    def add_mod(self, dn, mod_tuple):
        """Add a change to the active transaction.

        This function is meant to be called by the object mapper
        layer, not by the user.
        """
	if dn in self.changes:
            # Remove outstanding changes for the same attribute.
            # FIXME: this requires a better algorithm, as it is dead slow.
	    for mod in self.changes[dn]:
                if mod[1] != mod_tuple[1]:
                    continue
                if mod[0] == ldap.MOD_ADD:
		    self.changes[dn].remove(mod)
		    if mod_tuple[0] == ldap.MOD_ADD:
			mod_tuple = (ldap.MOD_ADD, mod_tuple[1], mod_tuple[2])
		    break
	    self.changes[dn].append(mod_tuple)
	else:
	    self.changes[dn] = [mod_tuple]
	if self.auto_commit:
	    self.commit()

    def rollback(self):
        """Roll back the current transaction.

        Any pending changes will be discarded.
        """
        self.changes = {}

    def commit(self):
        """Commit the active transaction to the database."""
	# nota: per via del funzionamento goffo di ldap, nel caso
	# in cui l'oggetto non esista ancora, non possiamo usare
	# ldap_modify con OP_ADD ma bisogna usare direttamente
	# ldap_add... sigh...
	for k, chg in self.changes.iteritems():
	    logging.debug("COMMIT: %s %s" % (str(k), str(chg)))
	    try:
		self.ldap.modify_s(k, chg)
	    except ldap.NO_SUCH_OBJECT:
		self.ldap.add_s(k, [(x[1], x[2]) for x in chg])
	self.changes = {}

